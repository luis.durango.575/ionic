// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};
/* configuracion de la base de datos*/
export const firebaseConfig = {
  apiKey: "AIzaSyA7fee0uxCtzo36ctVGjf4-2LNj7c8vD5A",
  authDomain: "miudb-eb9ee.firebaseapp.com",
  databaseURL: "https://miudb-eb9ee.firebaseio.com",
  projectId: "miudb-eb9ee",
  storageBucket: "miudb-eb9ee.appspot.com",
  messagingSenderId: "9106438292",
  appId: "1:9106438292:web:ca6560773cf0ae2974dc81",
  measurementId: "G-2WPD2Q3R5Q"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
