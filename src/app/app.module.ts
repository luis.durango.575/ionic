import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { firebaseConfig } from '../environments/environment'; /*se importa la base de dates desde firebase*/
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth'; /*Modulo de autenticacion de fireabse*/
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore'; /*se ultiza el modulo para ingresar al storage de firebase */
import { ChatComponent } from './componentes/chat/chat.component';
import {FormsModule} from '@angular/forms';
import { ModalComponent } from './componentes/modal/modal.component';






@NgModule({
  declarations: [AppComponent, ChatComponent, ModalComponent],
  entryComponents: [ChatComponent, ModalComponent],
  imports: [
    FormsModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule, AngularFirestoreModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: FirestoreSettingsToken, useValue: {} }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
