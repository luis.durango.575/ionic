import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../servicios/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;

  email: string;
  password: string;

  constructor(private authService: AuthService, public router: Router) { }

  ngOnInit() {
    
  }
  onSubmitLogin() {

    this.authService.login(this.email, this.password).then(res => {
      this.router.navigate(['/ejemplo-listas']);
    }).catch(err => alert('Los datos son incorrectos, intente de nuevo'))
  }

}
