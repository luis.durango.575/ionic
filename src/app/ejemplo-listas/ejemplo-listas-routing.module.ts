import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EjemploListasPage } from './ejemplo-listas.page';

const routes: Routes = [
  {
    path: '',
    component: EjemploListasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EjemploListasPageRoutingModule {}
