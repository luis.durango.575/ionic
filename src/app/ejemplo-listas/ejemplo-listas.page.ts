import { Component, OnInit } from '@angular/core';
import { url } from 'inspector';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from '../componentes/modal/modal.component';


@Component({
  selector: 'app-ejemplo-listas',
  templateUrl: './ejemplo-listas.page.html',
  styleUrls: ['./ejemplo-listas.page.scss'],
})
export class EjemploListasPage implements OnInit {
  listaInformacion = [];
  constructor(public modal: ModalController) {
    this.listaInformacion = [
      {
        hobbie: 'Ingenieria Informatica',
        prioridad: 'eye-off',
        icono: "wifi",
        descripcion: "correr",
        url:"assets/img/ingenieria.jpg",
        ico2:"eye"
      },
      {
        hobbie: 'Ingenieria Civil',
        prioridad: 'eye-off',
        icono: "wine",
        descripcion: "correr",
        url:"assets/img/civil.jpg",
        ico2:"eye"

      },
      {
        hobbie: 'Ingenieria Aeronautica',
        prioridad: 'eye-off',
        icono: "warning",
        descripcion: "correr",
        url:"assets/img/aeronáutica.jpeg",
        ico2:"eye"
      },
      {
        hobbie: 'Ginecologo',
        prioridad: 'eye-off',
        icono: "walk",
        descripcion: "correr",
        url:'assets/img/gine.jpg',
        ico2:"eye"
      }


    ]
   }

  ngOnInit() {
  }
openModal(){
this.modal.create({
  component: ModalComponent
 
}).then( (modal)=> modal.present())
}
}
