import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EjemploListasPageRoutingModule } from './ejemplo-listas-routing.module';
import { EjemploListasPage } from './ejemplo-listas.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EjemploListasPageRoutingModule
  ],
  declarations: [EjemploListasPage]
})
export class EjemploListasPageModule {}
