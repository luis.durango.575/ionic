import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EjemploListasPage } from './ejemplo-listas.page';

describe('EjemploListasPage', () => {
  let component: EjemploListasPage;
  let fixture: ComponentFixture<EjemploListasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemploListasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EjemploListasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
