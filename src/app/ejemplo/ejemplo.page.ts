import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ejemplo',
  templateUrl: './ejemplo.page.html',
  styleUrls: ['./ejemplo.page.scss'],
})
export class EjemploPage implements OnInit {

  titulo: string;
  nombre: string;
  cambia = false;
  listaInformacion = [];

  constructor() {

    this.titulo = 'Estudiante';
    this.nombre = 'Andres Durango Rondon';
    this.listaInformacion = [
      {
        hobbie: 'Ingenieria Informatica',
        prioridad: '1',
        icono: "wifi",
        descripcion: "correr"
      },
      {
        hobbie: 'Ingenieria Civil',
        prioridad: '2',
        icono: "wine",
        descripcion: "correr"
      },
      {
        hobbie: 'Ingenieria Aeronautica',
        prioridad: '3',
        icono: "warning",
        descripcion: "correr"
      },
      {
        hobbie: 'Ginecologo',
        prioridad: '4',
        icono: "walk",
        descripcion: "correr"
      }


    ]
  }

  ngOnInit() {
  }

  intercalar() {
    if (this.cambia) {
      this.nombre = 'Estudiante';
      this.titulo = 'Andres Durango Rondon';
      this.cambia = false;
    } else {
      this.titulo = 'Estudiante';
      this.nombre = 'Andres Durango Rondon';
      this.cambia = true;
    }

  }
  
}
