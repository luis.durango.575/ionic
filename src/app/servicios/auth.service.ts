import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { promise } from 'protractor';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';




@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private AFauth: AngularFireAuth, private router: Router, private firedb: AngularFirestore) { }


  /*Se crea el metodo para ingresar datos y autentificar con el metodo de firebase auth*/
  login(email: string, password: string) {

    return new Promise((resolve, rejected) => {
      this.AFauth.auth.signInWithEmailAndPassword(email, password).then(user => {
        resolve(user);
      }).catch(err => rejected(err));
    });


  }

  logout() {
    this.AFauth.auth.signOut().then(auth => {
      this.router.navigate(['/folder/Inbox']);
    })

  }
  register(email: string, password: string, name: string) {
    return new Promise((resolve, reject) => {
      this.AFauth.auth.createUserWithEmailAndPassword(email, password).then(res => {

        const uid = res.user.uid;
        this.firedb.collection('user').doc(uid).set({
          name: name,
          uid: uid
        })
        resolve(res)
      }).catch(err => reject(err))
    });


  }
}