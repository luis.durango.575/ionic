export const soccer_data = [
    {
        telefonos: [
          3114560585,
          3114560589,
          3114560580
        ],
        lesionado: true,
        raza: 'negro',
        numeroCamiseta: 99,
        nacionalidad: 'colombiano',
        manager: {
          nombreManager: 'pablo',
          telefonoManager: 'Elkin',
          direccionManager: 'pablo',
          nacionalidadManager: 'elkin'
        },
        nombres: 'jhon',
        apellidos: 'velez hernandez',
        historial: [
          {
            nombreEquipo: 'medellin',
            pais: 'colombia',
            titulos: 6
          },
          {
            nombreEquipo: 'junior',
            pais: 'colombia',
            titulos: 9
          }
        ]
      }
    ]
